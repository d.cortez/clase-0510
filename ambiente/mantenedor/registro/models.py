from django.db import models
from django.utils import timezone

# Create your models here.
class Alumno(models.Model):
    rut=models.CharField(max_length=10)
    nombre=models.CharField(max_length=100)
    apellido=models.CharField(max_length=100)
    carrera=models.CharField(max_length=200)
    fecha_nacimiento=models.DateTimeField(
        blank=True,null=True)

    """def __init__(self):
        self.rut="111"
        self.nombre=""
    """
    def __str__(self):
        return self.rut
